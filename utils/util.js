// var HOST = 'http://thare-lam.com:32848'
var HOST = 'http://127.0.0.1:32848'

var REQUEST = function(urlHash, requestParameter, storageKey){
  var me = this
  
  wx.request({
      url: HOST + urlHash,
      method: 'post',
      data: requestParameter,
      success: function(res) {
        var data = res.data
        if(data.status){
          me.setData({
            errorHidden: true,
            requestData: data
          })
          try{
            wx.setStorageSync(storageKey, data)
          }catch(e){
            console.log(e)
          }
        } else {
          me.setData({
            errorHidden: false,
            error: res.data.msg,
          })
        }
      },
      fail: function(res){
        console.log('fail');
        me.setData({
          errorHidden: false,
          error: res.errMsg,
          requestData: res.data
        })
      },
      complete: function(res) {
        wx.hideNavigationBarLoading()
      }
  })
}

module.exports = {
  host: HOST,
  request: REQUEST
}