App({
  onLaunch: function () {
    
  },
  getUserInfo:function(cb){
    var me = this
    wx.login({
      success: function () {
        wx.getUserInfo({
          success: function (res) {
            me.globalData.userInfo = res.userInfo
            typeof cb == "function" && cb(me.globalData.userInfo)
          }
        })
      }
    })
  },
  globalData:{
    userInfo:null
  }
})