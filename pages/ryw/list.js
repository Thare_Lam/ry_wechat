var util = require('../../utils/util.js')
var detailHash = '/ryw'
var storageKey = 'ryw'

Page({
  data: {
    errorHidden: true
  },
  onLoad:function(){
    var me = this
    var data = wx.getStorageSync(storageKey)
    if (data) {
      me.setData({
        requestData: data
      })
    }
    wx.showNavigationBarLoading()
    util.request.call(me, detailHash, {type: 'subjectbox'}, storageKey)
  },
  onPullDownRefresh: function() {
    wx.showNavigationBarLoading()
    util.request.call(this, detailHash, {type: 'subjectbox'}, storageKey)
  },
  articleClick: function(event) {
    var url = event.currentTarget.dataset.url;
    var title = event.currentTarget.dataset.title;
    wx.navigateTo({
      url: 'detail?url=' + url + '&title=' + title
    })
  }
})