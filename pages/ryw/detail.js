var util = require('../../utils/util.js')
var detailHash = '/ryw/detail'

Page({
  data:{
    loadingHidden: false,
    errorHidden: true  
  },
  onLoad:function(options){
    this.setData({
      articleUrl: options.url,
      articleTitle: options.title
    })
    util.request.call(this, detailHash, {link: this.data.articleUrl})
  },
  onPullDownRefresh: function() {
    this.setData({
      loadingHidden: false
    })
    util.request.call(this, detailHash, {link: this.data.articleUrl})
  },
  onReady:function(){
    wx.setNavigationBarTitle({
      title: this.data.articleTitle
    })
  }
})