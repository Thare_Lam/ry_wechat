var util = require('../../utils/util.js')
var detailHash = '/xxpt'
var storageKey = 'xxpt'

Page({
  data: {
    errorHidden: true
  },
  onLoad:function(){
    var me = this
    var data = wx.getStorageSync(storageKey)
    if (data) {
      me.setData({
        requestData: data
      })
    }
    wx.showNavigationBarLoading()
    util.request.call(me, detailHash, {}, storageKey)
  },
  onPullDownRefresh: function() {
    wx.showNavigationBarLoading()
    util.request.call(this, detailHash, {}, storageKey)
  },
  articleClick: function(event) {
    var id = event.currentTarget.dataset.id
    var title = event.currentTarget.dataset.title
    wx.navigateTo({
      url: 'detail?id=' + id + '&title=' + title
    })
  }
})