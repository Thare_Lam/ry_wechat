var util = require('../../utils/util.js')
var detailHash = '/xxpt/detail'

Page({
  data:{
    loadingHidden: false,
    errorHidden: true  
  },
  onLoad:function(options){
    this.setData({
      articleId: options.id,
      articleTitle: options.title
    })
    util.request.call(this, detailHash, {id: this.data.articleId})
  },
  onPullDownRefresh: function() {
    this.setData({
      loadingHidden: false
    })
    util.request.call(this, detailHash, {id: this.data.articleId})
  },
  onReady:function(){
    wx.setNavigationBarTitle({
      title: this.data.articleTitle
    })
  }
})