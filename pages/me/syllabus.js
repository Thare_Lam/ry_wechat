var util = require('../../utils/util.js')
var detailHash = '/xxpt/syllabus'
var storageKey = 'syllabus'

function getTerms(){
    var terms = []
    for (var i = 2013; i < 2019; i++) {
        for (var j = 0; j < 3; j++) {
            terms.push(i + '-' + j)
        }
    }
    return terms
}
Page({
    data: {
        terms: getTerms()
    },
    onLoad: function(){
        var me = this
        wx.showNavigationBarLoading();
        var data = wx.getStorageSync(storageKey);
        util.request.call(me, detailHash, {}, storageKey);
        // 将周几与上午下午数据分开
        if(data)
        {
           me.setData({
                week: data.data.syllabus[0],
                morningClass: data.data.syllabus[1],
                noonClass: data.data.syllabus[2],
                nightClass: data.data.syllabus[3]
            })
        }
    },
    termPickerChange: function(e){
        var me = this
        var index = parseInt(e.detail.value, 10)
        var term = me.data.requestData.data.all_terms[index]
        util.request.call(me, detailHash, {term: term}, storageKey)
    }
})