var app = getApp()
Page({
  data: {
    userInfo: {}
  },
  onLoad: function () {
    var me = this
    app.getUserInfo(function(userInfo){
      me.setData({
        userInfo:userInfo
      })
    })
  }
})
